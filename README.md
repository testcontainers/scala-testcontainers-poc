[![pipeline status](https://gitlab.com/testcontainers/scala-testcontainers-poc/badges/master/pipeline.svg)](https://gitlab.com/testcontainers/scala-testcontainers-poc/commits/master)

# testcontainers.org POC

The singular aim of this repo is to provide a POC for using testcontainers.org in gitlab CI.