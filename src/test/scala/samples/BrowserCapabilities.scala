package samples

import org.openqa.selenium.remote.DesiredCapabilities

trait BrowserCapabilities {
  def desiredCapabilities: DesiredCapabilities = sys.props.get("webdriver.capabilities") match {
    case Some("firefox") => DesiredCapabilities.firefox()
    case _ => DesiredCapabilities.chrome()
  }
}
