package samples

import org.scalatest.selenium.WebBrowser
import org.scalatest.{FeatureSpec, GivenWhenThen, Matchers}

class PocContainerSpec extends FeatureSpec with Matchers with GivenWhenThen with SeleniumBrowserRecordingSuite with WebBrowser with BrowserCapabilities {
  info("As a testcontainers user")
  info("I should be able to run via a gitlab pipeline")

  container.container.withStartupAttempts(2)

  feature("POC Container") {
    scenario("Browse apple.com") {
      Given("I browse to apple.com")
      go to "https://apple.com"

      When("I click on Music")
      click on linkText("Music")

      Then("The page title should be Apple - Music")
      pageTitle should be ("Music - Apple")
    }
  }
}
