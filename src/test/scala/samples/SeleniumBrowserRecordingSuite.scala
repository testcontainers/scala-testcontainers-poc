package samples

import java.io.File

import com.dimafeng.testcontainers.SeleniumTestContainerSuite
import org.scalatest.Suite
import org.testcontainers.containers.BrowserWebDriverContainer

trait SeleniumBrowserRecordingSuite extends Suite with SeleniumTestContainerSuite {
  override def recordingMode: (BrowserWebDriverContainer.VncRecordingMode, File) =
    (BrowserWebDriverContainer.VncRecordingMode.RECORD_ALL, reflect.io.File("target/captures").createDirectory().jfile)
}
